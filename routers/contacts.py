from flask import Blueprint, render_template, request, redirect
from models.contact import Contact
from utils.db import db

contacts = (Blueprint('contacts',__name__))

@contacts.route('/')
def list():
    contacts = Contact.query.all()
    return render_template('index.html', contacts = contacts)

@contacts.route('/new', methods=['POST'])
def add_contact():
    fullname = request.form['fullname']
    email = request.form['email']
    phone = request.form['phone']

    new_contact = Contact(fullname,email,phone)
    db.session.add(new_contact)
    db.session.commit()
    
    print(new_contact)

    return redirect('/')

@contacts.route('/update')
def update_contact():
    return "Update a Contact"

@contacts.route('/delete')
def delete_contact():
    return "Delete a contact"