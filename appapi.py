from flask import Flask
from routers.contacts import contacts
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:3053559355@localhost/api_flask'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.register_blueprint(contacts)

SQLAlchemy(app)

'''
if __name__ == '__main__':
    app.run(debug= True)  '''
    
    