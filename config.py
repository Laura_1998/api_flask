from distutils.command.config import config


class DevelopmentConfig():
    DEBUG = True
    MYSQL_HOST = 'localhost'
    MYSQL_USER = 'root'
    MYSQL_PASSWORD = '3053559355'
    MYSQL_DB = 'api_flask'

config = {
    'development': DevelopmentConfig
}